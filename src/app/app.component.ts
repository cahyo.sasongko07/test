import { Component, ViewChild } from "@angular/core";
import { App, Platform, AlertController, Nav } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { LoginPage } from "../pages/login/login";
import { TodoListPage } from "../pages/todo-list/todo-list";
import { CloudSharingPage } from "../pages/cloud-sharing/cloud-sharing";
import { OutletPage } from "../pages/outlet/outlet";
import { NearByPage } from "../pages/near-by/near-by";
import { ChangePasswordPage } from "../pages/change-password/change-password";
import { ProfilePage } from "../pages/profile/profile";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import { Geolocation } from "@ionic-native/geolocation";

export interface MenuItem {
  title: string;
  component: any;
  icon: any;
}

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  private pages: any;
  rootPage: any = LoginPage;
  public users: any;

  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public app: App,
    public alertCtrl: AlertController,
    public http: HttpClient,
    private geolocation: Geolocation
  ) {
    this.initializeApp();

    this.appMenuItems = [
      { title: "Todo List", component: TodoListPage, icon: "list-box" },
      { title: "Cloud Sharing", component: CloudSharingPage, icon: "share" },
      { title: "Outlet", component: OutletPage, icon: "cart" },
      { title: "Nearby", component: NearByPage, icon: "search" }
    ];

    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        this.updateLocationSales(resp.coords.latitude, resp.coords.longitude);
      })
      .catch(error => {
        console.log("Error getting location", error);
      });
  }

  updateLocationSales(lat, lng) {
    let body: FormData = new FormData();
    body.append("active", "1");
    body.append("lat", lat);
    body.append("lng", lng);
    body.append("users_id", JSON.parse(localStorage.getItem("id")));

    this.http
      .post(
        "http://150.242.111.235/apibonita/index.php/mwa/upLocationSales",
        body
      )
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        }
      );
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      // Control statusbar
      this.statusBar.styleDefault();

      // Control Splash Screen
      this.splashScreen.hide();

      // Close this application
      this.platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();
        if (activeView.name === "BonitaMWAPage") {
          const alert = this.alertCtrl.create({
            title: "App termination",
            message: "Do you want to close the app?",
            buttons: [
              {
                text: "Cancel",
                role: "cancel",
                handler: () => {
                  console.log("Application exit prevented!");
                }
              },
              {
                text: "Close App",
                handler: () => {
                  this.platform.exitApp();
                }
              }
            ]
          });
          alert.present();
        } else {
          nav.pop();
        }
      });
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }
  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Failed");
          }
        },
        {
          text: "Yes",
          handler: () => {
            localStorage.clear();
            this.nav.push(LoginPage);
          }
        }
      ]
    });
    confirm.present();
  }

  changePassword() {
    this.nav.push(ChangePasswordPage);
  }

  profile() {
    this.nav.push(ProfilePage);
  }

  getUsers() {
    let i = JSON.parse(localStorage.getItem("id"));
    let data: Observable<any>;
    data = this.http.get(
      "http://150.242.111.235/apibonita/index.php/profile/getProfile/" + i
    );
    data.subscribe(result => {
      this.users = result;
      console.log(this.users);
    });
  }
}
