// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Network } from "@ionic-native/network";
import { Headers, Http} from "@angular/http";
import "rxjs/add/operator/map";

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let apiUrl = "https://bonita.telkomcel.tl/apibonita/index.php/Auth/login/";

@Injectable()
export class AuthServiceProvider {
  constructor(public http: Http, private network: Network) {
    console.log("Hello AuthServiceProvider Provider");
  }
  isOnline(): boolean {
    console.log(this.network.type);
    if (this.network.type != "none") {
      return true;
    } else {
      return false;
    }
  }
  postData(credentials, type) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      this.http
        .post(apiUrl + type, JSON.stringify(credentials), {headers: headers})
        .subscribe(
          res => {
            resolve(res.json());
          },
          err => {
            reject(err);
          }
        );
    });
  }
}
