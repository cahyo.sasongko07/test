import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {

  //local
  public urlRoot:string = "http://localhost/apibonita/index.php/";
  public url:string = "http://localhost/apibonita/";

  //deploy
  // public urlRoot:string = "http://150.242.111.235/apibonita/index.php/";
  // public url:string = "http://150.242.111.235/apibonita/";

  //pruduction
  // public urlRoot:string = "https://bonita.telkomcel.tl/apibonita/index.php/";
  // public url:string = "https://bonita.telkomcel.tl/apibonita/";

  constructor(public http: HttpClient) {
    console.log('Hello ServiceProvider Provider');
  }

}
