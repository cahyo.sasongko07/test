import { Component,ViewChild } from '@angular/core';
import { NavController, Navbar } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { UpdateOutletProfilePage } from "../update-outlet-profile/update-outlet-profile";
import { AddoutletPage } from "../addoutlet/addoutlet";
import { BonitaMWAPage } from '../bonita-mwa/bonita-mwa';
import { ServiceProvider } from "../../providers/service/service";

@Component({
  selector: "page-outlet",
  templateUrl: "outlet.html"
})

// getMunicipio,getPosto,getSuco,getLaldy
export class OutletPage {
  @ViewChild(Navbar) navBar: Navbar;
  public items: any = [];
  
  public order:any = 0;
  public lim:any = 20;
  
  public order2:any = 0;
  public lim2:any = 20;
  
  myInput:any;
  constructor(public navCtrl: NavController, public http: HttpClient,     public service : ServiceProvider) {
    
  }
  
  ionViewDidLoad() {
    this.loadData();
    this.navBar.backButtonClick = (e:UIEvent)=>{
      // todo something
      this.navCtrl.setRoot(BonitaMWAPage);
    }
  }
  
  ionViewWillEnter(){
  }
  
  doRefresh(refresher) {
    this.loadData();
    setTimeout(() => {
      refresher.complete();
    }, 300);
  }
  
  loadData() {
    this.items = [];
    let data: Observable<any>;
    data = this.http.get(this.service.urlRoot+"mwa/getOutletMWA?cluster="+localStorage.getItem('cl')+'&limit='+this.lim+'&order=0');
    data.subscribe(result => {
      if(result.success == true){
        
        for (var i = 0; i < result.data.length; ++i) {
          this.items.push({
            id: result.data[i].id,
            name: result.data[i].name,
            img: this.cekImg(result.data[i].outlet_photo)
          });
        }
      }
    });
  }
  
  cekImg(img) {
    if (img !== "null" && img !== null) {
      return this.service.url+"uploads/" + img;
    } else {
      return "assets/img/users.png";
    }
  }
  
  toGetOutlet(d) {
    this.navCtrl.push(UpdateOutletProfilePage, {
      id: d
    });
  }
  
  addOutlet() {
    this.navCtrl.push(AddoutletPage);
  }
  
  doInfinite(infiniteScroll) {
    
    if(this.myInput != '' && this.myInput != undefined){
      this.order2 = (this.order2 + this.lim2);  
      this.order = 0;
      setTimeout(() => {
        let data: Observable<any>;
        data = this.http.get(this.service.urlRoot+"mwa/findOutletMwa?cluster="+localStorage.getItem('cl')+"&cari="+this.myInput+"&limit="+this.lim2+"&order="+this.order2);
        data.subscribe(result => {
          if(result.success == true){
            
            for (var i = 0; i < result.data.length; ++i) {
              this.items.push({
                id: result.data[i].id,
                name: result.data[i].name,
                img: this.cekImg(result.data[i].outlet_photo)
              });
            }
            
          }
        });
        infiniteScroll.complete();
      }, 300);
    }else{
      this.order2 = 0;
      setTimeout(() => {
        this.order = (this.order + this.lim); 
        let data: Observable<any>;
        data = this.http.get(this.service.urlRoot+"mwa/getOutletMwa?cluster="+localStorage.getItem('cl')+"&limit="+this.lim+"&order="+this.order);
        data.subscribe(result => {
          if(this.order > result.count){
            console.log('gak bisa reload lagi bos');
          }else{
            if(result.success == true){
              for (var i = 0; i < result.data.length; ++i) {
                this.items.push({
                  id: result.data[i].id,
                  name: result.data[i].name,
                  img: this.cekImg(result.data[i].outlet_photo)
                });
              }
            } 
          }
        });
        infiniteScroll.complete();
      }, 300);
    }
  }
  
  onInput(event){
    if(this.myInput == ''){
      this.order = 0;
      this.loadData();
    }else{
      this.items = [];
      let data: Observable<any>;
      data = this.http.get(this.service.urlRoot+"mwa/findOutletMwa?cluster="+localStorage.getItem('cl')+"&cari="+this.myInput+"&limit="+this.lim2+"&order=0");
      data.subscribe(result => {
        if(result.success == true){
          for (var i = 0; i < result.data.length; ++i) {
            this.items.push({
              id: result.data[i].id,
              name: result.data[i].name,
              img: this.cekImg(result.data[i].outlet_photo)
            });
          }
        }
      });
    }
  }
}
