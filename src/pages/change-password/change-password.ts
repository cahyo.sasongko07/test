import { Component,ViewChild } from "@angular/core";
import { NavController } from "ionic-angular";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { AlertController } from "ionic-angular";
import { ServiceProvider } from "../../providers/service/service";

@Component({
  selector: "page-change-password",
  templateUrl: "change-password.html"
})
export class ChangePasswordPage {
  constructor(
    public service : ServiceProvider,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public http: HttpClient
  ) {
    console.log(localStorage.getItem('id'));
  }
 
  @ViewChild('oldpassword') oldpassword;
  @ViewChild('password') password;
  @ViewChild('newpassword') newpassword;

  public items:any;
  
  changePassword() {
    let data:Observable<any>;
    let postData = new FormData();

    postData.append('oldpassword',this.oldpassword.value);
    postData.append('password',this.password.value);
    postData.append('id',localStorage.getItem('id'));
    postData.append('newpassword',this.newpassword.value);

    data = this.http.post(this.service.urlRoot+"mwa/change_password", postData);
    data.subscribe(result => {
      this.items = result;
      console.log(this.items);
      if (this.items['action'] == 1) {
        this.alerts('Error','Your old password was entered incorrectly. Please enter it again.');
      }else if (this.items['action'] == 2) {
        this.alerts('Error','Password do not match!');
      }else if(this.items['action'] == 3){
        this.alerts('Error','Your new password can not be the same as old password.');
      }else if(this.items['action'] == 4){
        this.alerts("Success", "Your password has been successfully changed.");
        this.navCtrl.pop();
      }
    })
  }

  showConfirm() {
    this.checkInput(this.oldpassword.value,this.password.value,this.newpassword.value);
  }

  checkInput(a,b,c) {
    if (a == "" || b == "" || c == "") {
      this.alerts('Input Required','Please enter your password!');
    }else{
        const confirm = this.alertCtrl.create({
        title: 'Change Password',
        message: 'Do you agree to change your password?',
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
               console.log('Failed');
            }
          },
          {
            text: 'Agree',
            handler: () => {
              this.changePassword();
            }
          }
        ]
      });
      confirm.present();
    }
  }

  alerts(t,st) {
    let alert = this.alertCtrl.create({
      title: t,
      subTitle: st,
      buttons: ['Dismiss']
    });
    alert.present();
}
}