import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StockSurveyPage } from './stock-survey';

@NgModule({
  declarations: [
    StockSurveyPage,
  ],
  imports: [
    IonicPageModule.forChild(StockSurveyPage),
  ],
})
export class StockSurveyPageModule {}
