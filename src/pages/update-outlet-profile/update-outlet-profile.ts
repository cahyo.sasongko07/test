import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { OutletPage } from '../outlet/outlet';
import { NearByPage } from '../near-by/near-by';
import { NearbyOutletsPage } from '../nearby-outlets/nearby-outlets';
import { ServiceProvider } from "../../providers/service/service";

@Component({
  selector: "page-update-outlet-profile",
  templateUrl: "update-outlet-profile.html"
})
export class UpdateOutletProfilePage {
  items:any;
  id:any;
  path:any;
  
  constructor( public navCtrl: NavController,   public service : ServiceProvider, public navParams: NavParams, public http: HttpClient){
    this.id = navParams.get("id");
    this.loadData();
    this.path =  this.service.url+'/uploads/outlets/';
  }
  
 loadData(){
    let data:Observable<any>;
   data = this.http.get(this.service.urlRoot+'mwa/getOutlets?id='+this.id);
    data.subscribe(result => {
      this.items = result;
      debugger;
    })
  }

  editOutlet(id){
    this.navCtrl.push("EditoutletPage",{id : id});
  }

  toNearby(lat='',lng=''){
    this.navCtrl.push(NearByPage,{lat : lat, lng : lng});
  }

}
