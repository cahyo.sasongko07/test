import { Component, ViewChild } from "@angular/core";
import { IonicPage,NavController, ToastController,NavParams, Navbar } from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { RoutePage } from "../route/route";
import { BonitaMWAPage } from "../bonita-mwa/bonita-mwa";
import { ServiceProvider } from "../../providers/service/service";
import * as moment from "moment";


@IonicPage()
@Component({
  selector: 'page-todo-list-detail',
  templateUrl: 'todo-list-detail.html',
})
export class TodoListDetailPage {
  public items: any = [];
  date:any = '';
  @ViewChild(Navbar) navBar: Navbar;

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public toastCtrl: ToastController,
    public service : ServiceProvider,
    private navParams: NavParams
  ) {
    this.date = moment(this.navParams.get('date')).format("MMMM, YYYY");
    let data: Observable<any>;
    // tabel = getTodolist
    data = this.http.get(this.service.urlRoot+"mwa/getRouteDataTask?id=" + JSON.parse(localStorage.getItem("id"))+'&date='+this.navParams.get('date'));
    data.subscribe(result => {
      for (var i = 0; i < result["data"].length; ++i) {
        this.items.push({
          task_name: result["data"][i].task_name,
          idRt: result["data"][i].route_trip_fk,
          idTask: result["data"][i].id,
          statusIncidentalTask: result["data"][i].statusIncidentalTask,
          status: this.getStatus(result["data"][i].status)
        });
      }
      console.log(this.items);
    });
  }

  ionViewDidLoad() {
    
  }

  goToRoute(idRt, idTask,statusIncidentalTask) {
    this.navCtrl.push(RoutePage, {
      id: idRt,
      statusIncidentalTask: statusIncidentalTask,
      idTask: idTask
    });
  }

  getStatus(x) {
    if (x === "1") {
      return "In Progress";
    } else if (x === "2") {
      return "Follow Up";
    } else if (x === "3") {
      return "Completed";
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });
    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });
    toast.present();
  }
}
