import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodoListDetailPage } from './todo-list-detail';

@NgModule({
  declarations: [
    TodoListDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(TodoListDetailPage),
  ],
})
export class TodoListDetailPageModule {}
