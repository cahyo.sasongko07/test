import { Component, Renderer2} from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RoutePage } from '../route/route';
import { ServiceProvider } from "../../providers/service/service";
import * as $ from "jquery";

/**
* Generated class for the SalesOrderPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-sales-order',
  templateUrl: 'sales-order.html',
})
export class SalesOrderPage {
  
  nominal:any = [];
  discount:any = [];
  unit_price:any = [];
  url:any = "http://150.242.111.235/apibonita/index.php/mwa/";
  jml:any;
  product:any;
  balance:any;
  default:any = 0;
  simcard:any = {};
  etopup:any = {};
  total:any = [];
  dollar:any = [];
  typeInput = this.navParams.get('typeInput');
  
  no: number = 0;
  
  start : number = 0;
  end : number = 0;
  qty : number = 0;
  
  arrStart :any;
  arrEnd :any;
  arrQty :any;

  countVoucher:any = [];

  cSimcard:number = 0;
  cVoucher:number[] = [];

  public anArray:any=[];
  public bnSimcard:any=[];
  public anVoucher:any[] = [];
  public bnVoucher:any=[];
  
  public rQty:any=[];
  public rStart:any=[];
  public rEnd:any=[];
  
  public vQty:any=[];
  public vStart:any=[];
  public vEnd:any=[];
  public vId:any=[];

  isDisSimcard : boolean;
  isDisVoucher : boolean;
  
  constructor(public http: HttpClient,private renderer:Renderer2,public service : ServiceProvider,public navCtrl: NavController,public loadingCtrl: LoadingController, public navParams: NavParams,private alertCtrl: AlertController) {
    this.getIDJProduct(2);
    this.getIDJProductSimcard(1);
    this.getIDJProductEtopup(3);
    console.log('Outlet : ',this.cekInputEndUser(localStorage.getItem('oId')));
  }
  
  ionViewDidLoad() {
    this.typeInput = this.getTypeInput();
    // console.log(this.arrStart);
  }

  cekInputEndUser(outlet=''){
    if (this.typeInput == 'i') {
      outlet = 'null';
    }

    return outlet;
  }
  
  changeBN(){
    let nStart:any =  this.start;
    let nQty:any =  this.qty;
    
    let nilai = parseInt(nStart) + parseInt(nQty);
    this.end = nilai;
  }
  
  Add(x=220897){
    
    if(x == 220897){
      
      if( (this.anArray.length + 1) > this.simcard.nominal){
        console.log("Gak bisa nambah dari lebih dari", this.simcard.nominal);
      }else{

      let no = this.no++;
      let cekInput:number = this.cSimcard - this.rQty[no];
      if(this.cSimcard != parseInt(this.simcard.nominal)){
        this.anArray.push(no);
      }else{
        console.log("Gak bisa lagi bro");
      }
      
      
      if(no > 0){
        let v1 = no - 1;
        this.rStart[no] = parseInt(this.rEnd[v1]) + 1;
      }
      
      this.go('r');
      }
    }else{
      this.seeVoucher(x);

      this.countVoucher[x] = [];
      for (let i = 0; i < this.anVoucher.length; i++) {
        if(this.anVoucher[i].id == x){
          console.log(this.anVoucher[i].id);
          this.countVoucher[x].push(this.anVoucher[i].id);
        }
      }

      if((this.countVoucher[x].length + 1) > this.nominal[x]){
        console.log("Gak bisa nambah dari lebih dari", this.nominal[x]);
      }else{

      let x1:any = x;
      let no = this.no++;
      
      let cekInput:number = this.cVoucher[x] - this.vQty[no];
      if(this.cVoucher[x] != parseInt(this.nominal[x])){
        this.anVoucher.push({
          id : parseInt(x1),
          n : no
        });
      }else{
        console.log("Gak bisa lagi bro");
      }
      
      
      // if(no > 0){
      //   let v1 = no - 1;
      //   this.vStart[no] = parseInt(this.vEnd[v1]) + 1;
      // }

      this.go('v');
    }
      
    }
    
  }
  
  go(x='r'){

    let arrQty = [];
    let arrStart = [];
    let arrEnd = [];

    let vouQty = [];
    let vouStart = [];
    let vouEnd = [];
    
    if(x == 'r'){
      
      for (let i = 0; i < this.rQty.length; i++) {
        if(this.rQty[i] != undefined ||this.rQty[i]  != NaN){
          arrQty.push(parseInt(this.rQty[i]));
        }
      }
      
      for (let i = 0; i < this.rStart.length; i++) {
        if(!isNaN(this.rStart[i])){
          arrStart.push(parseInt(this.rStart[i]));
        }
      }
      
      for (let i = 0; i < this.rEnd.length; i++) {
        if(this.rEnd[i] != undefined ||this.rEnd[i]  != NaN){
          arrEnd.push(parseInt(this.rEnd[i]));
        }
      }
      
    }else if(x == 'v'){

    }
    
  }
  
  jumlahkan(v,s,id){
    if(s == 0){
      let rQty = parseInt(this.rQty[v]);
      let rStart = parseInt(this.rStart[v]);
      
      this.rEnd[v] =  rQty + rStart - 1;
      this.seeSimcard();
      this.isDisSimcard = this.cekAddInput(this.cSimcard,parseInt(this.simcard.nominal));

      let cekInput:number = this.cSimcard - this.rQty[v];
      let validasiQty = parseInt(this.simcard.nominal) - cekInput;
  
      if(this.rQty[v] > parseInt(this.simcard.nominal)){
        this.rQty[v] = '';
      }else if(this.rQty[v] > validasiQty){
        this.rQty[v] = '';
      }else if(this.rQty[v] < 0 || this.rQty[v] == 0){
        this.rQty[v] = '';
      }

      console.log("Cek Bro :",this.isDisSimcard);

    }else if(s == 1){
      let vQty = parseInt(this.vQty[v]);
      let vStart = parseInt(this.vStart[v]);
      
      this.vEnd[v] =  vQty + vStart - 1;

      this.seeVoucher(id);
      this.isDisVoucher = this.cekAddInput(this.cVoucher[id],parseInt(this.nominal[id]));

      let cekInput:number = this.cVoucher[id] - this.vQty[v];
      let validasiQty = parseInt(this.nominal[id]) - cekInput;
  
      if(this.vQty[v] > parseInt(this.nominal[id])){
        this.vQty[v] = '';
      }else if(this.vQty[v] > validasiQty){
        this.vQty[v] = '';
      }else if(this.vQty[v] < 0 || this.vQty[v] == 0){
        this.vQty[v] = '';
      }
      console.log("Cek Voucher Bro :",this.isDisVoucher);

    }
  }

cekAddInput(x,y){
  let bool:boolean;
  if(x >= y){
    bool = true;
  }else{
    bool = false;
  }
  return bool;
}
  
  funInfun(v){
    if(this.rEnd[v] == undefined){
      this.funInfun(v--);
    }else{
      console.log(this.rEnd[v]);
      return v;
    }
  }
  
  removeElement(id,qty,start,end,s){
    if(s == 0){
      console.log('parameter : ',id,this.rQty[id],this.rStart[id],this.rEnd[id])
      // $(document).ready(function(){
      //     $("#inp"+idx).remove("#inp"+idx);
      // });
      
      
      // // Umum
      this.rmElement(0,0,id,s);
      
      if(qty == undefined || start == undefined || end == undefined){
        console.log('tidak bisa');
      }else{
        // Qty  
        this.rmElement(1,qty,id,s);
        // Start
        this.rmElement(2,start,id,s);
        // End
        this.rmElement(3,end,id,s);
      }
      
      
      // console.log('sRemove = arrUmum : ',this.anArray);
      // console.log('sRemove = Qty : ',this.rQty);
      // console.log('sRemove = Start : ',this.rStart);
      // console.log('sRemove = End : ',this.rEnd);
    }else if(s == 1){
      console.log('parameter : ',id,this.vQty[id],this.vStart[id],this.vEnd[id])
      // $(document).ready(function(){
      //     $("#inp"+idx).remove("#inp"+idx);
      // });
      
      
      // // Umum
      this.rmElement(0,0,id,s);
      
      if(qty == undefined || start == undefined || end == undefined){
        console.log('tidak bisa');
      }else{
        // Qty  
        this.rmElement(1,qty,id,s);
        // Start
        this.rmElement(2,start,id,s);
        // End
        this.rmElement(3,end,id,s);
      }
      
      
      // console.log('sRemove = arrUmum : ',this.anArray);
      // console.log('sRemove = Qty : ',this.rQty);
      // console.log('sRemove = Start : ',this.rStart);
      // console.log('sRemove = End : ',this.rEnd);
    }
   
  }
  
  
  rmElement(no,val,id,s){
    let rm ;
    
    
    if(no == 0 && s ==0){
      rm = this.anArray.indexOf(id);
      if (rm > -1) {
        this.anArray.splice(rm,1);
      }
      
    console.log('No Rm : '+rm);
      
    }else if(s == 1){
      rm = this.anVoucher.findIndex(this.indexOf,id)
      if (rm > -1) {
        this.anVoucher.splice(rm,1);
      }
      
      console.log('No Rm : '+rm);
    }
    
    
  }
  
  seeVoucher(id=0){
    this.bnVoucher = [];
    let o:any = 0;
    console.log('Hitung Voucher : ',this.anVoucher.length);
    for (let i = 0; i < this.anVoucher.length; i++) {
      console.log('Hasil Benar '+this.anVoucher[i].id+':',this.vQty[this.anVoucher[i].n],this.vStart[this.anVoucher[i].n],this.vEnd[this.anVoucher[i].n]);
      this.bnVoucher.push({
        'id' : parseInt(this.anVoucher[i].id),
        'qty' : parseInt(this.vQty[this.anVoucher[i].n]),
        'start' : parseInt(this.vStart[this.anVoucher[i].n]),
        'end' : parseInt(this.vEnd[this.anVoucher[i].n])  
      });

      if(!isNaN(this.vQty[this.anVoucher[i].n]) && this.anVoucher[i].id == id){
        o += parseInt(this.vQty[this.anVoucher[i].n]);
        this.cVoucher[this.anVoucher[i].id] = o;
      }
    }
    
  }

  seeSimcard(){
    this.bnSimcard = [];
    let o:any = 0;
    console.log('Hitung : ',this.anArray.length);
    for (let i = 0; i < this.anArray.length; i++) {
      console.log('Hasil Benar :',this.rQty[this.anArray[i]],this.rStart[this.anArray[i]],this.rEnd[this.anArray[i]]);
      this.bnSimcard.push({
        'qty' : parseInt(this.rQty[this.anArray[i]]),
        'start' : parseInt(this.rStart[this.anArray[i]])  ,
        'end' : parseInt(this.rEnd[this.anArray[i]])  
      });
      if(!isNaN(this.rQty[this.anArray[i]])){
        o += parseInt(this.rQty[this.anArray[i]]);
        this.cSimcard = o;
      }
      console.log("Hasil Count simcard : ",  this.cSimcard);
      console.log('Hasil bnSimcard : '+ this.bnSimcard);
    }

  }

  btnBN(){
    this.seeVoucher();
    this.seeSimcard();
    let blockNumber:any = [];
    blockNumber.push({
      'bnSimcard' : this.bnSimcard,      
      'bnVoucher' : this.bnVoucher,      
    })
  }

   indexOf(element) {
    return element.n == this;
  }
  
  addBN(){
    this.no += 1;
    let no = this.no;
    $(document).ready(function(){
      
      $("#bn").children("input").each(function(){
        alert($(this).attr("id") + " has a value of " + $(this).val());
      });
      
      $('#bn').append('<tr id="viewBN'+no+'"><td><ion-input type="number"  class="input input-md" ng-reflect-type="number"><input  id="qty'+no+'" name="qty[]" class="text-input text-input-md" dir="auto" ng-reflect-klass="text-input" ng-reflect-ng-class="text-input-md" type="number" autocomplete="off" autocorrect="off" placeholder="" ng-reflect-type="number"></ion-input></td><td><ion-input type="number"  class="input input-md" ng-reflect-type="number"><input id="start'+no+'" name="start[]" class="text-input text-input-md" dir="auto" ng-reflect-klass="text-input" ng-reflect-ng-class="text-input-md" type="number" autocomplete="off" autocorrect="off" placeholder="" ng-reflect-type="number"></ion-input></td><td><ion-input type="number" class="input input-md" ng-reflect-type="number"><input  id="end'+no+'" name="end[]"  class="text-input text-input-md" dir="auto" ng-reflect-klass="text-input" ng-reflect-ng-class="text-input-md" type="number" autocomplete="off" autocorrect="off" placeholder="" ng-reflect-type="number"></ion-input></td><td width: 10px;><button ion-button="" id="rmBN'+no+'" style="background: #f53d66;"  class="disable-hover button button-md button-default button-default-md"><span class="button-inner"><ion-icon ios="ios-add-circle" md="md-add-circle" role="img" class="icon icon-md ion-md-trash" aria-label="add circle" ng-reflect-ios="ios-add-circle" ng-reflect-md="md-add-circle"></ion-icon></span><div class="button-effect" style="transform: translate3d(-17px, -12px, 0px) scale(1); height: 70px; width: 70px; opacity: 0; transition: transform 277ms ease 0s, opacity 194ms ease 83ms;"></div></button></td></tr>');
      
      $( "#rmBN"+no ).click(function() {
        $("#viewBN"+no).remove("#viewBN"+no);
      });
      
      $("#start"+no).keyup(function(){
        let qty:any = $("#qty"+no).val();
        let start:any = $("#start"+no).val();
        let end:any = $("#end"+no).val();
        
        let nStart = parseInt(start);
        let nQty = parseInt(qty);
        let nEnd = parseInt(end);
        
        let nilai:number = nStart + nQty;
        
        $("#end"+no).val(nilai);
      });
      
      $("#qty"+no).keyup(function(){
        
        let end:any = $("#end"+no).val();
        
        if(no > 1){
          let no2 = no - 1;
          end = $("#end"+no2).val();
          
          var x = $('#end'+no);
          
          console.log(x);
          
          $("#start"+no).val(parseInt(end) + 1);  
        }
        
        setTimeout( () => {
          let qty:any = $("#qty"+no).val();
          let start:any = $("#start"+no).val();
          let nStart = parseInt(start);
          let nQty = parseInt(qty);
          
          let nilai:number = nStart + nQty;
          $("#end"+no).val(nilai);
        },300);
      });
      
      let arrQty =  this.arrQty = [];
      let arrStart =  this.arrStart = [];
      let arrEnd =  this.arrEnd = [];
      
      $('input[name="qty[]"]').each(function() {
        arrQty.push({
          qty : $(this).val(),
        });
      });
      
      $('input[name="start[]"]').each(function() {
        arrStart.push({
          start : $(this).val(),
        });
      });
      
      $('input[name="end[]"]').each(function() {
        arrEnd.push({
          end : $(this).val(),
        });
      });
      
      
    });
  }
  
  getTypeInput(){
    if(this.typeInput == undefined){
      this.typeInput = 'c';
    }else{
      this.typeInput = this.typeInput;
    }
    return this.typeInput;
  }
  
  changeValue(id){
    let voucher = [];
    
    let nom = this.nominal.length;
    let dis = this.discount.length;
    let jml = 0;
    
    if(nom != 0){
      jml = nom;
    }else if(dis != 0){
      jml = dis;
    }
    
    for (var i = 0; i < jml; ++i) {
      if(this.nominal[i] != undefined || this.discount[i] != undefined){
        this.dollar[i] = (this.nominal[i] * this.unit_price[i] *  this.discount[i]) / 100
        this.total[i] = ((this.nominal[i] * this.unit_price[i]) - this.dollar[i]);
      }
    }
    
    // Not Looping
    this.simcard.dollar = (this.simcard.nominal * this.simcard.price *  this.simcard.discount) / 100; 
    this.simcard.total = (this.simcard.nominal * this.simcard.price - this.simcard.dollar);
    
    this.etopup.dollar = (this.etopup.nominal * this.etopup.price * this.etopup.discount) / 100; 
    this.etopup.total = (this.etopup.nominal * this.etopup.price - this.etopup.dollar);
  }
  
  getIDJProduct(id) {
    this.product = [];
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      let d:any = data; 
      for (let i = 0; i < d.length; i++) {
        
        this.total[d[i].id] = '0';
        this.unit_price[d[i].id] = '0';
        this.discount[d[i].id] = '0';
        this.dollar[d[i].id] = '0';
        this.nominal[d[i].id] = '0';
        
        this.product.push({
          id : d[i].id,
          unit_price : d[i].unit_price,
          discount : d[i].discount,
          product_name : d[i].product_name,
          value : '0'
        });
      }
    });
  }
  
  getIDJProductSimcard(id) {
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      this.simcard.price = data[0].unit_price;
      this.simcard.discount = data[0].discount;
    });
  }
  
  getIDJProductEtopup(id) {
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      this.etopup.price = data[0].unit_price;
      this.etopup.discount = data[0].discount;
    });
  }
  
  toSalesOrder(){
    
    let loading = this.loadingCtrl.create({
      content: "Please Wait.."
    });
    loading.present();
    
    let voucher = [];
    
    let nom = this.nominal.length;
    let dis = this.discount.length;
    let jml = 0;
    
    if(nom != 0){
      jml = nom;
    }else if(dis != 0){
      jml = dis;
    }
    
    for (var i = 0; i < jml; ++i) {
      if(this.nominal[i] != undefined || this.discount[i] != undefined){
        voucher.push({ 
          nominal : this.nominal[i],
          discount : this.discount[i],
          idProduct : i,
          dollar : this.dollar[i],
          idJp : 2,
          total: this.total[i]
        });
      }
    }
    
    let s = this.simcard;
    let e = this.etopup;
    
    let SE = {
      simcard : [{
        nominal : this.validValue(s.nominal),
        discount : this.validValue(s.discount),
        idProduct : 9999,
        dollar : s.dollar, 
        total : s.total, 
        idJp : 1
      }],
      etopup : [{
        nominal : this.validValue(e.nominal),
        discount : this.validValue(e.discount),
        idProduct : 6666,
        dollar : e.dollar,
        total : e.total, 
        idJp : 3
      }],
    }
    
    // Block Number
    this.seeVoucher();
    this.seeSimcard();

    let blockNumber:any = [];
    blockNumber.push({
      'bnSimcard' : this.bnSimcard,      
      'bnVoucher' : this.bnVoucher,      
    })

    let data = new FormData;
    data.append('SE', JSON.stringify(SE));
    data.append('order', JSON.stringify(voucher));
    data.append('idu', localStorage.getItem('id'));
    data.append('idCh', localStorage.getItem('idCh'));
    data.append('oId', this.cekInputEndUser(localStorage.getItem('oId')));
    data.append('idTask', localStorage.getItem('idTask'));
    data.append('idRtd', localStorage.getItem('idRtd'));
    data.append('stId', localStorage.getItem('stId'));
    data.append('blockNumber', JSON.stringify(blockNumber));
    data.append('typeInput', this.getTypeInput());
    data.append('balance', this.balance);
    this.http.post(this.service.urlRoot+'mwa/saveSalesOrder',data).subscribe( d => {
      let alert = this.alertCtrl.create({
        title: 'Info',
        subTitle: 'The Data Survey has been Added',
        buttons: ['Okay']
      });
      alert.present();
      loading.dismiss();
      
      this.navCtrl.push(RoutePage, {
        'id' : localStorage.getItem('idFromRoot'),
        'idTask' : localStorage.getItem('idTask')
      });
    });
    
  }
  
  validValue(v){
    if(v == undefined){
      v = '';  
    }
    return v;
  }
  
}
