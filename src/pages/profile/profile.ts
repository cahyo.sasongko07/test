import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { RestProvider } from "../../providers/rest/rest";
import { ServiceProvider } from "../../providers/service/service";

@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  public users: any;
  constructor(
    public navCtrl: NavController,
    public restProvider: RestProvider,
    public http: HttpClient,
    public service : ServiceProvider

  ) { this.getUsers(); }
  getUsers() {
       let i = JSON.parse(localStorage.getItem('id'));
    let data:Observable<any>;
    data = this.http.get(this.service.urlRoot+'profile/getProfile/'+i);
     data.subscribe(result => {
       this.users = result;
       console.log(this.users);
     })
  }
}
