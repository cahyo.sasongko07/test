import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { RoutePage } from '../route/route';
import { StockSurveyPage } from '../stock-survey/stock-survey';
import { ViewActivityPage } from '../view-activity/view-activity';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the CheckinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkin',
  templateUrl: 'checkin.html',
})
export class CheckinPage {
  d : any;n:any;
  aksi:any = [];
  point : any = [];
  task_id : any = [];
  survey_id : any = [];
  idRt:any = this.navParams.get('idRt');
  id:any = this.navParams.get('idTask');
  idRtd:any = this.navParams.get('idRtd');

  constructor(public service : ServiceProvider,public navCtrl: NavController,private loading :LoadingController, public navParams: NavParams,public http: HttpClient,private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckinPage');
    this.getCheckin();
  }

  getCheckin(){
  	let data:Observable<any>;
  	data = this.http.get(this.service.urlRoot+'mwa/getCheckin?idTask='+this.id+'&idRtd='+this.idRtd);
  	data.subscribe(result => {
  		this.n = 0;
  		this.d = result;
  		console.log(this.d);
  	});
  }

  save(){
  	let d = [];
  	for (var i = 0; i < this.point.length; ++i) {
  		if (this.point[i] != undefined || this.point[i] != null  ) {
  			d.push({
  				answer : this.point[i],
  				task_id : this.task_id[i],
  				survey_id : this.survey_id[i], 
          outlet_id : localStorage.getItem('oId'),
  				idRtd : this.idRtd,
  			});
  		}
  	}
    console.log(d);
    let loading = this.loading.create({
      content: "Please Wait.."
    });
  	for (var i = 0; i < d.length; ++i) {
  		this.http.post(this.service.urlRoot+"mwa/saveCheckin", JSON.stringify(d[i]))
	  	.subscribe(res => {
       this.aksi.push({i : res});
	  	console.log(res);
	  	}, (err) => {
       this.aksi.push({i : err});
	 		console.log(err);
	  	});
  	};

    console.log(this.aksi.length);
	  if (this.aksi) {
      loading.dismiss();
        this.toSurveyStock();
     }

  }

  /* SURVEY STOK */

  toSurveyStock(){
    this.navCtrl.push(ViewActivityPage, {
      id: localStorage.getItem('idTask'),
      idRtd: localStorage.getItem('idRtd')
    });
  }

}
