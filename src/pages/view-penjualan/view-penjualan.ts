import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { SalesOrderPage } from '../sales-order/sales-order';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the StockSurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-penjualan',
  templateUrl: 'view-penjualan.html',
})
export class ViewPenjualanPage {
  url:any = "http://150.242.111.235/apibonita/index.php/mwa/";
  product:any = [];
  voucher:any = [];
  simcard:any = {};
  etopup:any = {};
  telkomcel:any = [0];
  telecom:any = [];
  telemor:any = [];

  vtelkomcel:any = [];
  vtelecom:any = [];
  vtelemor:any = [];
  no:any = 1;

  constructor(public navCtrl: NavController,public service : ServiceProvider,public loadingCtrl: LoadingController,public http: HttpClient, public navParams: NavParams) {
    this.getIDJProduct(4);
    this.getIDJVoucher(2);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StockSurveyPage');
  }

  getIDJProduct(id) {
    this.product = [];
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      let d:any = data; 
      for (let i = 0; i < d.length; i++) {
       
        this.telkomcel[d[i].id] = '0';
        this.telecom[d[i].id] = '0';
        this.telemor[d[i].id] = '0';

        this.product.push({
          id : d[i].id,
          product_name : d[i].product_name,
          value : '0'
        });
    }
    });
  }

  getIDJVoucher(id) {
    this.voucher = [];
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      let d:any = data;
      for (let i = 0; i < d.length; i++) {
        
        this.vtelkomcel[d[i].id] = '0';
        this.vtelecom[d[i].id] = '0';
        this.vtelemor[d[i].id] = '0';

          this.voucher.push({
            id : d[i].id,
            product_name : d[i].product_name,
            value : '0'
          });
      }
    });
  }

  // TO

  buildStockArray(){

    let bund = [];
    let vouc = [];
    let s = this.simcard;
    let e = this.etopup;
    let jml;
    let jml2;

    if(this.telkomcel.length != 0){
      jml = this.telkomcel.length;
    }else if(this.telecom.length != 0){
      jml = this.telecom.length;
    }else if(this.telemor.length != 0){
      jml = this.telemor.length;
    }

    if(this.vtelkomcel.length != 0){
      jml2 = this.vtelkomcel.length;
    }else if(this.vtelecom.length != 0){
      jml2 = this.vtelecom.length;
    }else if(this.vtelemor.length != 0){
      jml2 = this.vtelemor.length;
    }

    for (var i = 0; i < jml; ++i) {
      if(this.telkomcel[i] != undefined || this.telemor[i] != undefined || this.telecom[i] != undefined){
      bund.push({ 
        telkomcel : this.telkomcel[i],
        telecom : this.telecom[i],
        telemor : this.telemor[i],
        idProduct : i,
        idJp : 4
      });
      }
    }

    for (var i = 0; i < jml2; ++i) {
      if(this.vtelkomcel[i] != undefined || this.vtelemor[i] != undefined || this.vtelecom[i] != undefined){
      vouc.push({ 
        telkomcel : this.vtelkomcel[i],
        telecom : this.vtelecom[i],
        telemor : this.vtelemor[i],
        idProduct : i,
        idJp : 2
      });
      }
    }

    let arr = {
      simcard : [{
        telkomcel : this.validValue(s.telkomcel),
        telecom : this.validValue(s.telecom),
        telemor : this.validValue(s.telemor),
        idProduct : 9999,
        idJp : 1
      }],
      etopup : [{
        telkomcel : this.validValue(e.telkomcel),
        telecom : this.validValue(e.telecom),
        telemor : this.validValue(e.telemor),
        idProduct : 6666,
        idJp : 3
      }],
      voucher : vouc,
      bundling : bund
    }
   
    return arr;
  }

  validValue(v){
    if(v == undefined){
      v = '';  
    }
    return v;
  }

  toSalesOrder(){
    let loading = this.loadingCtrl.create({
      content: "Please Wait.."
    });
    loading.present();

    console.log(this.buildStockArray());
    let data = new FormData;
    data.append('stock', JSON.stringify(this.buildStockArray()));
    data.append('idCh', localStorage.getItem('idCh'));
    data.append('idTask', localStorage.getItem('idTask'));
    data.append('oId', localStorage.getItem('oId'));
    data.append('stId', localStorage.getItem('stId'));
    this.http.post(this.service.urlRoot+'mwa/saveSurveiPenjualan',data).subscribe( d => {
        loading.dismiss();
        this.navCtrl.push("SalesOrderPage");
    });
  }
  
}
