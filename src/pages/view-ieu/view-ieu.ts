import { Component } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the ViewIeuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-ieu',
  templateUrl: 'view-ieu.html',
})
export class ViewIeuPage {

  stock:any = {};
  spenjualan:any = {};
  order:any = {};
  viewActivity:any;
  survey:any;
  menu:any;

  oId = localStorage.getItem('oId');
  bnSimcard: any;
  bnVoucher: any;
  constructor(public navCtrl: NavController,private http: HttpClient,public service : ServiceProvider, public navParams: NavParams) {
    this.menu = "sStock";
    this.getSalesOrder();
  }

  ionViewDidLoad() {
    this.getBlockNumberSimcard(this.oId,1);
    this.getBlockNumberVoucher(this.oId,2);
  }

  getSalesOrder(){
    this.http.get(this.service.urlRoot+'mwa/getSalesOrder?idTask='+this.navParams.get("idTask")).subscribe(data => {
      this.order.simcard = data['simcard'];
      this.order.etopup = data['etopup'];
      this.order.voucher = data['voucher'];
    });
  }

  getBlockNumberSimcard(id,jp){
    this.http.get(this.service.urlRoot+'mwa/getBlockNumber?jp='+jp+'&id_tasks='+localStorage.getItem('idTask')).subscribe(data => {
      let d:any = data;
       this.bnSimcard = d.bnSimcard;
    });
  }

  getBlockNumberVoucher(id,jp){
    this.http.get(this.service.urlRoot+'mwa/getBlockNumber?jp='+jp+'&id_tasks='+localStorage.getItem('idTask')).subscribe(data => {
      let d:any = data;
       this.bnVoucher = d.bnVoucher;
    });
  }

}
