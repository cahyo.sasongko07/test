import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewIeuPage } from './view-ieu';

@NgModule({
  declarations: [
    ViewIeuPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewIeuPage),
  ],
})
export class ViewIeuPageModule {}
